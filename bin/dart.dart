import 'dart:io';
import 'dart:math';

void main(List<String> inputpost) {
  stdout.write("input the infix : ");
  String args = stdin.readLineSync()!;
  stdout.write("postfix is : ");

  print(tokenStr(args));
  List tkenize = tokenStr(args);
  List post = inTofix(tkenize);
  stdout.write("from infix to postfix : ");
  print(post);
  stdout.write("Evaluate Postfix : ");
  print(evaluatePostfix(post));
}

List tokenStr(String input) {
  List<String> tokenList = [];
  String tmp = "";
  for (var i = 0; i < input.length; i++) {
    if (input[i] == " ") {
      if (tmp.isNotEmpty) {
        tokenList.add(tmp);
        tmp = "";
      }
    } else {
      tmp += input[i];
    }
  }
  if (!input[input.length - 1].contains(" ")) {
    tokenList.add(tmp);
  }
  return tokenList;
}

// converting infix to postfix method
List inTofix(List input) {
  List<String> OperatorList = [];
  List<String> listPost = [];
  List<String> CheckOperator = ["+", "-", "^", "*", "/"];
  var OperatVal = {"+": 1, "-": 1, "^": 3, "*": 2, "/": 2};
  infixToPostfix(input, CheckOperator, OperatorList, OperatVal, listPost);
  return listPost;
}

// evaluate method
num evaluatePostfix(List A) {
  List<num> vals = [];
  var right, left, ans;
  popAndDelete(A, vals, right, left, ans);
  return vals[0];
}

void infixToPostfix(
    List<dynamic> input,
    List<String> CheckOperator,
    List<String> OperatorList,
    Map<String, int> OperatVal,
    List<String> listPost) {
  for (var i = 0; i < input.length; i++) {
    if (CheckOperator.contains(input[i])) {
      while (OperatorList.isNotEmpty &&
          OperatorList.last != "(" &&
          OperatVal[input[i]]! <= OperatVal[OperatorList.last]!) {
        listPost.add(OperatorList.last);
        OperatorList.removeLast();
      }
      OperatorList.add(input[i]);
    } else if (input[i] == "(" || input[i] == ")") {
      if (input[i] == "(") {
        OperatorList.add(input[i]);
      }
      if (input[i] == ")") {
        while (OperatorList.last != "(") {
          listPost.add(OperatorList.last);
          OperatorList.removeLast();
        }
        OperatorList.removeLast();
      }
    } else {
      if (int.parse(input[i]) is int) {
        listPost.add(input[i]);
      }
    }
  }

  while (OperatorList.isNotEmpty) {
    listPost.add(OperatorList.last);
    OperatorList.removeLast();
  }
}

void popAndDelete(List<dynamic> A, List<num> vals, right, left, ans) {
  for (var args in A) {
    if (!checkOperator(args)) {
      vals.add(double.parse(args));
    } else {
      right = vals.last;
      vals.removeLast();
      left = vals.last;
      vals.removeLast();
      applyOperator(args, ans, left, right, vals);
    }
  }
}

void applyOperator(args, ans, left, right, List<num> vals) {
  if (args == "+") {
    ans = left + right;
    vals.add(ans);
  } else if (args == "-") {
    ans = left - right;
    vals.add(ans);
  } else if (args == "*") {
    ans = left * right;
    vals.add(ans);
  } else if (args == "/") {
    ans = left / right;
    vals.add(ans);
  } else if (args == "^") {
    ans = pow(left, right);
    vals.add(ans);
  }
}

bool checkOperator(String x) {
  if (x == "+" || x == "-" || x == "*" || x == "/" || x=="^") {
    return true;
  }
  return false;
}
